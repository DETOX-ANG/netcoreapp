﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreApp.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Uname { get; set; }
        public string PWord { get; set; }
    }
}
