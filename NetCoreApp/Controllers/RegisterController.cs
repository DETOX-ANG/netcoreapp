﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetCoreApp.Models;

namespace NetCoreApp.Controllers
{
    public class RegisterController : Controller
    {
        private readonly AppDBContext _context;

        public RegisterController(AppDBContext context) {

            _context = context;

        }
        [HttpGet]
        public IActionResult Register() {

            return View();
        }


        [HttpPost]
        public IActionResult Registra(User user)
        {
            _context.User.Add(user);
            _context.SaveChanges();

            return View();
        }
    }
}